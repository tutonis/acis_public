package net.sf.l2j.commons.mmocore;

/**
 * @author KenM
 * @param <T>
 */
public interface IMMOExecutor<T extends MMOClient<?>> {
  void execute(ReceivablePacket<T> packet);
}
