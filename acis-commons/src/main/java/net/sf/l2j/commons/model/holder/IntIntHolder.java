package net.sf.l2j.commons.model.holder;

/** A generic int/int container. */
public class IntIntHolder {
  private int _id;
  private int _value;

  public IntIntHolder(int id, int value) {
    _id = id;
    _value = value;
  }

  public int getId() {
    return _id;
  }

  public void setId(int id) {
    _id = id;
  }

  public int getValue() {
    return _value;
  }

  public void setValue(int value) {
    _value = value;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + ": Id: " + _id + ", Value: " + _value;
  }
}
