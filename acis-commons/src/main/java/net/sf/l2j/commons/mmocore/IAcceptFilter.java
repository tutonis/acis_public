package net.sf.l2j.commons.mmocore;

import java.nio.channels.SocketChannel;

/** @author KenM */
public interface IAcceptFilter {
  boolean accept(SocketChannel sc);
}
