# l2jacis project

Modernising build process would consist of multiple equally important goals that  need to be individually fulfilled:
  * modernise tech
  * streamline development story
  * have clear build/usage story for non dev people
  * write comprehensive tutorials

Modernise tech:
  * have clear dependency and app structure
  * port build process to gradle
  * remove legacy crust
  * have somewhat reasonable docker story for advanced users

Preferred project output when building `acis_public`:
  * builds *gameserver* app with all its dependencies
  * builds *loginserver* app with all its dependencies
  * builds helper apps as jars (or one manager):
    * account manager
    * geodataconverter
    * gsregistering
  * copies all resources into one location:
    * jars
    * configs
    * scripts

## what has been done:

 * delete unused *mmocore* module
 * delete personal eclipse config
 * add gitignores
 * add readme.md files
 * start on modernising folder structure
   * move config to root
   * move db tools to root
   * move server scripts to root
 * applied google-java-formatter to unify style - its not great, but its sure as hell consistent across developers/ides/universes

## extracted commons module

  * config to commons
  * model.location to commons
  * IntIntHolder to commons
  * refactored IntIntHolder.getSkill coz of dependency to gameserver
  * 

## remaining problems:

  * datapack building - we need to abstract datapack conversion before building gameserver
  * building process for other customs

### docker:

For *mysql*:
  * port docker run script to *docker-compose*
  * simplify mysql image and mount *volume for sql scripts*

### future updates

There are few low hanging fruits:
  * migrate to *hikari* cp - its safer, its faster
    [link1](https://talk.openmrs.org/t/using-hikaricp-instead-of-c3p0/1972/8) and
    [link2](https://github.com/brettwooldridge/HikariCP) and etc

### notes to myself

* https://github.com/JoeAlisson/async-mmocore - nice
* release/branching strategy
* mirror to github script
* collect all l2j server projects form github/gitlab/bitbucket with notes if they have something nice


