package net.sf.l2j.acis.cli.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountManagerTest {

  private AccountManager accountManager;

  @BeforeEach
  public void setup() {
    this.accountManager = new AccountManager();
  }

  @Test
  public void testRunningManager() {
    accountManager.start();
  }
}
