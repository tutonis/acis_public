package net.sf.l2j.acis.cli;

import net.sf.l2j.acis.cli.manager.AccountManager;
import net.sf.l2j.acis.cli.manager.ServerRegisterManager;

public class AcisCli {

    private final AccountManager accountManager;
    private final ServerRegisterManager serverRegisterManager;

    public AcisCli() {
        this.accountManager = new AccountManager();
        this.serverRegisterManager = new ServerRegisterManager();
    }
}
