package net.sf.l2j.acis.cli.manager;

public interface Startable {

    void start();

}
