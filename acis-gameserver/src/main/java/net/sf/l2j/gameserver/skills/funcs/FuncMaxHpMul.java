package net.sf.l2j.gameserver.skills.funcs;

import net.sf.l2j.gameserver.skills.Env;
import net.sf.l2j.gameserver.skills.Formulas;
import net.sf.l2j.gameserver.skills.Stats;
import net.sf.l2j.gameserver.skills.basefuncs.Func;

public class FuncMaxHpMul extends Func {
  static final FuncMaxHpMul _fmhm_instance = new FuncMaxHpMul();

  private FuncMaxHpMul() {
    super(Stats.MAX_HP, 0x20, null, null);
  }

  public static Func getInstance() {
    return _fmhm_instance;
  }

  @Override
  public void calc(Env env) {
    env.mulValue(Formulas.CON_BONUS[env.getCharacter().getCON()]);
  }
}
