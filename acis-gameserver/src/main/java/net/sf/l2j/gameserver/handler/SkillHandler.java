package net.sf.l2j.gameserver.handler;

import java.util.HashMap;
import java.util.Map;
import net.sf.l2j.gameserver.handler.skillhandlers.*;
import net.sf.l2j.gameserver.templates.skills.L2SkillType;

public class SkillHandler {
  private final Map<Integer, ISkillHandler> _datatable = new HashMap<>();

  protected SkillHandler() {
    registerSkillHandler(new BalanceLife());
    registerSkillHandler(new Blow());
    registerSkillHandler(new Cancel());
    registerSkillHandler(new CombatPointHeal());
    registerSkillHandler(new Continuous());
    registerSkillHandler(new CpDamPercent());
    registerSkillHandler(new Craft());
    registerSkillHandler(new Disablers());
    registerSkillHandler(new DrainSoul());
    registerSkillHandler(new Dummy());
    registerSkillHandler(new Extractable());
    registerSkillHandler(new Fishing());
    registerSkillHandler(new FishingSkill());
    registerSkillHandler(new GetPlayer());
    registerSkillHandler(new GiveSp());
    registerSkillHandler(new Harvest());
    registerSkillHandler(new Heal());
    registerSkillHandler(new HealPercent());
    registerSkillHandler(new InstantJump());
    registerSkillHandler(new Manadam());
    registerSkillHandler(new ManaHeal());
    registerSkillHandler(new Mdam());
    registerSkillHandler(new Pdam());
    registerSkillHandler(new Resurrect());
    registerSkillHandler(new Sow());
    registerSkillHandler(new Spoil());
    registerSkillHandler(new StrSiegeAssault());
    registerSkillHandler(new SummonFriend());
    registerSkillHandler(new Sweep());
    registerSkillHandler(new TakeCastle());
    registerSkillHandler(new Unlock());
  }

  public static SkillHandler getInstance() {
    return SingletonHolder._instance;
  }

  public void registerSkillHandler(ISkillHandler handler) {
    for (L2SkillType t : handler.getSkillIds()) _datatable.put(t.ordinal(), handler);
  }

  public ISkillHandler getSkillHandler(L2SkillType skillType) {
    return _datatable.get(skillType.ordinal());
  }

  public int size() {
    return _datatable.size();
  }

  private static class SingletonHolder {
    protected static final SkillHandler _instance = new SkillHandler();
  }
}
