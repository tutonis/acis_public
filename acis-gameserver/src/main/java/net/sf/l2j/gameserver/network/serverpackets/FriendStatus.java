package net.sf.l2j.gameserver.network.serverpackets;

import net.sf.l2j.gameserver.data.PlayerNameTable;
import net.sf.l2j.gameserver.model.World;

/**
 * Support for "Chat with Friends" dialog. <br>
 * Inform player about friend online status change <br>
 * Format: cdSd<br>
 * d: Online/Offline<br>
 * S: Friend Name <br>
 * d: Player Object ID <br>
 *
 * @author JIV
 */
public class FriendStatus extends L2GameServerPacket {
  private final boolean _online;
  private final int _objid;
  private final String _name;

  public FriendStatus(int objId) {
    _objid = objId;
    _name = PlayerNameTable.getInstance().getPlayerName(objId);
    _online = World.getInstance().getPlayer(objId) != null;
  }

  @Override
  protected final void writeImpl() {
    writeC(0x7b);
    writeD(_online ? 1 : 0);
    writeS(_name);
    writeD(_objid);
  }
}
