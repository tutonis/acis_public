package net.sf.l2j.gameserver.handler;

import java.util.HashMap;
import java.util.Map;
import net.sf.l2j.gameserver.handler.usercommandhandlers.*;

public class UserCommandHandler {
  private final Map<Integer, IUserCommandHandler> _datatable = new HashMap<>();

  protected UserCommandHandler() {
    registerUserCommandHandler(new ChannelDelete());
    registerUserCommandHandler(new ChannelLeave());
    registerUserCommandHandler(new ChannelListUpdate());
    registerUserCommandHandler(new ClanPenalty());
    registerUserCommandHandler(new ClanWarsList());
    registerUserCommandHandler(new DisMount());
    registerUserCommandHandler(new Escape());
    registerUserCommandHandler(new Loc());
    registerUserCommandHandler(new Mount());
    registerUserCommandHandler(new OlympiadStat());
    registerUserCommandHandler(new PartyInfo());
    registerUserCommandHandler(new SiegeStatus());
    registerUserCommandHandler(new Time());
  }

  public static UserCommandHandler getInstance() {
    return SingletonHolder._instance;
  }

  public void registerUserCommandHandler(IUserCommandHandler handler) {
    for (int id : handler.getUserCommandList()) _datatable.put(id, handler);
  }

  public IUserCommandHandler getUserCommandHandler(int userCommand) {
    return _datatable.get(userCommand);
  }

  public int size() {
    return _datatable.size();
  }

  private static class SingletonHolder {
    protected static final UserCommandHandler _instance = new UserCommandHandler();
  }
}
