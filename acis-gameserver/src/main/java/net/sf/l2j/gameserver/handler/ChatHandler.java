package net.sf.l2j.gameserver.handler;

import java.util.HashMap;
import java.util.Map;
import net.sf.l2j.gameserver.handler.chathandlers.*;

public class ChatHandler {
  private final Map<Integer, IChatHandler> _datatable = new HashMap<>();

  protected ChatHandler() {
    registerChatHandler(new ChatAll());
    registerChatHandler(new ChatAlliance());
    registerChatHandler(new ChatClan());
    registerChatHandler(new ChatHeroVoice());
    registerChatHandler(new ChatParty());
    registerChatHandler(new ChatPartyMatchRoom());
    registerChatHandler(new ChatPartyRoomAll());
    registerChatHandler(new ChatPartyRoomCommander());
    registerChatHandler(new ChatPetition());
    registerChatHandler(new ChatShout());
    registerChatHandler(new ChatTell());
    registerChatHandler(new ChatTrade());
  }

  public static ChatHandler getInstance() {
    return SingletonHolder._instance;
  }

  public void registerChatHandler(IChatHandler handler) {
    for (int id : handler.getChatTypeList()) _datatable.put(id, handler);
  }

  public IChatHandler getChatHandler(int chatType) {
    return _datatable.get(chatType);
  }

  public int size() {
    return _datatable.size();
  }

  private static class SingletonHolder {
    protected static final ChatHandler _instance = new ChatHandler();
  }
}
