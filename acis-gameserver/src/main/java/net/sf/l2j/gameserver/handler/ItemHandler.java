package net.sf.l2j.gameserver.handler;

import java.util.HashMap;
import java.util.Map;
import net.sf.l2j.gameserver.handler.itemhandlers.*;
import net.sf.l2j.gameserver.model.item.kind.EtcItem;

public class ItemHandler {
  private final Map<Integer, IItemHandler> _datatable = new HashMap<>();

  protected ItemHandler() {
    registerItemHandler(new BeastSoulShot());
    registerItemHandler(new BeastSpice());
    registerItemHandler(new BeastSpiritShot());
    registerItemHandler(new BlessedSpiritShot());
    registerItemHandler(new Book());
    registerItemHandler(new Calculator());
    registerItemHandler(new Elixir());
    registerItemHandler(new EnchantScrolls());
    registerItemHandler(new FishShots());
    registerItemHandler(new Harvester());
    registerItemHandler(new ItemSkills());
    registerItemHandler(new Keys());
    registerItemHandler(new Maps());
    registerItemHandler(new MercTicket());
    registerItemHandler(new PaganKeys());
    registerItemHandler(new PetFood());
    registerItemHandler(new Recipes());
    registerItemHandler(new RollingDice());
    registerItemHandler(new ScrollOfResurrection());
    registerItemHandler(new SeedHandler());
    registerItemHandler(new SevenSignsRecord());
    registerItemHandler(new SoulShots());
    registerItemHandler(new SpecialXMas());
    registerItemHandler(new SoulCrystals());
    registerItemHandler(new SpiritShot());
    registerItemHandler(new SummonItems());
  }

  public static ItemHandler getInstance() {
    return SingletonHolder._instance;
  }

  public void registerItemHandler(IItemHandler handler) {
    _datatable.put(handler.getClass().getSimpleName().intern().hashCode(), handler);
  }

  public IItemHandler getItemHandler(EtcItem item) {
    if (item == null || item.getHandlerName() == null) return null;

    return _datatable.get(item.getHandlerName().hashCode());
  }

  public int size() {
    return _datatable.size();
  }

  private static class SingletonHolder {
    protected static final ItemHandler _instance = new ItemHandler();
  }
}
