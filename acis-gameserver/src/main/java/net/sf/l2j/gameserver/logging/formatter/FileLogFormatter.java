package net.sf.l2j.gameserver.logging.formatter;

import java.util.logging.LogRecord;
import net.sf.l2j.gameserver.logging.MasterFormatter;

public class FileLogFormatter extends MasterFormatter {
  @Override
  public String format(LogRecord record) {
    return "["
        + getFormatedDate(record.getMillis())
        + "]"
        + SPACE
        + record.getLevel().getName()
        + SPACE
        + record.getMessage()
        + CRLF;
  }
}
