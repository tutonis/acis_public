package net.sf.l2j.gameserver.network.serverpackets;

import java.util.Calendar;
import net.sf.l2j.gameserver.data.sql.ClanTable;
import net.sf.l2j.gameserver.model.actor.instance.Player;
import net.sf.l2j.gameserver.model.entity.Castle;
import net.sf.l2j.gameserver.model.pledge.Clan;

/**
 * Shows the Siege Info<br>
 * <br>
 * packet type id 0xc9<br>
 * format: cdddSSdSdd<br>
 * <br>
 * c = c9<br>
 * d = CastleID<br>
 * d = Show Owner Controls (0x00 default || >=0x02(mask?) owner)<br>
 * d = Owner ClanID<br>
 * S = Owner ClanName<br>
 * S = Owner Clan LeaderName<br>
 * d = Owner AllyID<br>
 * S = Owner AllyName<br>
 * d = current time (seconds)<br>
 * d = Siege time (seconds) (0 for selectable)<br>
 * d = (UNKNOW) Siege Time Select Related?
 *
 * @author KenM
 */
public class SiegeInfo extends L2GameServerPacket {
  private final Castle _castle;

  public SiegeInfo(Castle castle) {
    _castle = castle;
  }

  @Override
  protected final void writeImpl() {
    Player activeChar = getClient().getActiveChar();
    if (activeChar == null) return;

    writeC(0xc9);
    writeD(_castle.getCastleId());
    writeD(
        ((_castle.getOwnerId() == activeChar.getClanId()) && (activeChar.isClanLeader()))
            ? 0x01
            : 0x00);
    writeD(_castle.getOwnerId());
    if (_castle.getOwnerId() > 0) {
      Clan owner = ClanTable.getInstance().getClan(_castle.getOwnerId());
      if (owner != null) {
        writeS(owner.getName()); // Clan Name
        writeS(owner.getLeaderName()); // Clan Leader Name
        writeD(owner.getAllyId()); // Ally ID
        writeS(owner.getAllyName()); // Ally Name
      } else _log.warning("Null owner for castle: " + _castle.getName());
    } else {
      writeS("NPC"); // Clan Name
      writeS(""); // Clan Leader Name
      writeD(0); // Ally ID
      writeS(""); // Ally Name
    }

    writeD((int) (Calendar.getInstance().getTimeInMillis() / 1000));
    writeD((int) (_castle.getSiege().getSiegeDate().getTimeInMillis() / 1000));
    writeD(0x00); // number of choices?
  }
}
