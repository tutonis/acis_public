package net.sf.l2j.gameserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import net.sf.l2j.Config;
import net.sf.l2j.L2DatabaseFactory;
import net.sf.l2j.commons.concurrent.ThreadPool;
import net.sf.l2j.commons.lang.StringUtil;
import net.sf.l2j.commons.mmocore.SelectorConfig;
import net.sf.l2j.commons.mmocore.SelectorThread;
import net.sf.l2j.commons.util.SysUtil;
import net.sf.l2j.gameserver.cache.CrestCache;
import net.sf.l2j.gameserver.cache.HtmCache;
import net.sf.l2j.gameserver.communitybbs.Manager.ForumsBBSManager;
import net.sf.l2j.gameserver.data.*;
import net.sf.l2j.gameserver.data.manager.BuyListManager;
import net.sf.l2j.gameserver.data.sql.BookmarkTable;
import net.sf.l2j.gameserver.data.sql.ClanTable;
import net.sf.l2j.gameserver.data.sql.ServerMemoTable;
import net.sf.l2j.gameserver.data.xml.*;
import net.sf.l2j.gameserver.geoengine.GeoEngine;
import net.sf.l2j.gameserver.handler.*;
import net.sf.l2j.gameserver.idfactory.IdFactory;
import net.sf.l2j.gameserver.instancemanager.*;
import net.sf.l2j.gameserver.instancemanager.games.MonsterRace;
import net.sf.l2j.gameserver.model.World;
import net.sf.l2j.gameserver.model.entity.Hero;
import net.sf.l2j.gameserver.model.olympiad.Olympiad;
import net.sf.l2j.gameserver.model.olympiad.OlympiadGameManager;
import net.sf.l2j.gameserver.model.partymatching.PartyMatchRoomList;
import net.sf.l2j.gameserver.model.partymatching.PartyMatchWaitingList;
import net.sf.l2j.gameserver.model.vehicles.*;
import net.sf.l2j.gameserver.network.L2GameClient;
import net.sf.l2j.gameserver.network.L2GamePacketHandler;
import net.sf.l2j.gameserver.scripting.ScriptManager;
import net.sf.l2j.gameserver.taskmanager.*;
import net.sf.l2j.gameserver.xmlfactory.XMLDocumentFactory;
import net.sf.l2j.util.DeadLockDetector;
import net.sf.l2j.util.IPv4Filter;

public class GameServer {
  private static final Logger _log = Logger.getLogger(GameServer.class.getName());
  private static GameServer _gameServer;
  private final SelectorThread<L2GameClient> _selectorThread;

  public GameServer() throws Exception {
    // Create log folder
    new File("./log").mkdir();
    new File("./log/chat").mkdir();
    new File("./log/console").mkdir();
    new File("./log/error").mkdir();
    new File("./log/gmaudit").mkdir();
    new File("./log/item").mkdir();
    new File("./data/crests").mkdirs();

    // Create input stream for log file -- or store file data into memory
    try (InputStream is = new FileInputStream(new File("config/logging.properties"))) {
      LogManager.getLogManager().readConfiguration(is);
    }

    StringUtil.printSection("aCis");

    // Initialize config
    Config.loadGameServer();

    // Factories
    XMLDocumentFactory.getInstance();
    L2DatabaseFactory.getInstance();

    StringUtil.printSection("ThreadPool");
    ThreadPool.init();

    StringUtil.printSection("IdFactory");
    IdFactory.getInstance();

    StringUtil.printSection("World");
    World.getInstance();
    MapRegionTable.getInstance();
    AnnouncementData.getInstance();
    ServerMemoTable.getInstance();

    StringUtil.printSection("Skills");
    SkillTable.getInstance();
    SkillTreeTable.getInstance();

    StringUtil.printSection("Items");
    ItemTable.getInstance();
    SummonItemData.getInstance();
    HennaData.getInstance();
    BuyListManager.getInstance();
    MultisellData.getInstance();
    RecipeTable.getInstance();
    ArmorSetData.getInstance();
    FishData.getInstance();
    SpellbookData.getInstance();
    SoulCrystalData.getInstance();
    AugmentationData.getInstance();
    CursedWeaponsManager.getInstance();

    StringUtil.printSection("Admins");
    AdminData.getInstance();
    BookmarkTable.getInstance();
    MovieMakerManager.getInstance();
    PetitionManager.getInstance();

    StringUtil.printSection("Characters");
    CharTemplateTable.getInstance();
    PlayerNameTable.getInstance();
    NewbieBuffData.getInstance();
    TeleportLocationData.getInstance();
    HtmCache.getInstance();
    PartyMatchWaitingList.getInstance();
    PartyMatchRoomList.getInstance();
    RaidBossPointsManager.getInstance();

    StringUtil.printSection("Community server");
    if (Config.ENABLE_COMMUNITY_BOARD) // Forums has to be loaded before clan data
    ForumsBBSManager.getInstance().initRoot();
    else _log.config("Community server is disabled.");

    StringUtil.printSection("Clans");
    CrestCache.getInstance();
    ClanTable.getInstance();
    AuctionManager.getInstance();
    ClanHallManager.getInstance();

    StringUtil.printSection("Geodata & Pathfinding");
    GeoEngine.getInstance();

    StringUtil.printSection("Zones");
    ZoneManager.getInstance();

    StringUtil.printSection("Task Managers");
    AttackStanceTaskManager.getInstance();
    DecayTaskManager.getInstance();
    GameTimeTaskManager.getInstance();
    ItemsOnGroundTaskManager.getInstance();
    MovementTaskManager.getInstance();
    PvpFlagTaskManager.getInstance();
    RandomAnimationTaskManager.getInstance();
    ShadowItemTaskManager.getInstance();
    WaterTaskManager.getInstance();

    StringUtil.printSection("Castles");
    CastleManager.getInstance();

    StringUtil.printSection("Seven Signs");
    SevenSigns.getInstance().spawnSevenSignsNPC();
    SevenSignsFestival.getInstance();

    StringUtil.printSection("Manor Manager");
    CastleManorManager.getInstance();

    StringUtil.printSection("NPCs");
    BufferTable.getInstance();
    HerbDropData.getInstance();
    NpcTable.getInstance();
    WalkerRouteData.getInstance();
    DoorTable.getInstance().spawn();
    StaticObjectData.getInstance();
    SpawnTable.getInstance();
    RaidBossSpawnManager.getInstance();
    GrandBossManager.getInstance();
    DayNightSpawnManager.getInstance();
    DimensionalRiftManager.getInstance();

    StringUtil.printSection("Olympiads & Heroes");
    OlympiadGameManager.getInstance();
    Olympiad.getInstance();
    Hero.getInstance();

    StringUtil.printSection("Four Sepulchers");
    FourSepulchersManager.getInstance().init();

    StringUtil.printSection("Quests & Scripts");
    ScriptManager.getInstance();

    if (Config.ALLOW_BOAT) {
      BoatManager.getInstance();
      BoatGiranTalking.load();
      BoatGludinRune.load();
      BoatInnadrilTour.load();
      BoatRunePrimeval.load();
      BoatTalkingGludin.load();
    }

    StringUtil.printSection("Events");
    MonsterRace.getInstance();

    if (Config.ALLOW_WEDDING) CoupleManager.getInstance();

    if (Config.ALT_FISH_CHAMPIONSHIP_ENABLED) FishingChampionshipManager.getInstance();

    StringUtil.printSection("Handlers");
    _log.config("AutoSpawnHandler: Loaded " + AutoSpawnManager.getInstance().size() + " handlers.");
    _log.config(
        "AdminCommandHandler: Loaded " + AdminCommandHandler.getInstance().size() + " handlers.");
    _log.config("ChatHandler: Loaded " + ChatHandler.getInstance().size() + " handlers.");
    _log.config("ItemHandler: Loaded " + ItemHandler.getInstance().size() + " handlers.");
    _log.config("SkillHandler: Loaded " + SkillHandler.getInstance().size() + " handlers.");
    _log.config(
        "UserCommandHandler: Loaded " + UserCommandHandler.getInstance().size() + " handlers.");

    StringUtil.printSection("System");
    Runtime.getRuntime().addShutdownHook(Shutdown.getInstance());
    ForumsBBSManager.getInstance();
    _log.config("IdFactory: Free ObjectIDs remaining: " + IdFactory.getInstance().size());

    if (Config.DEADLOCK_DETECTOR) {
      _log.info("Deadlock detector is enabled. Timer: " + Config.DEADLOCK_CHECK_INTERVAL + "s.");

      final DeadLockDetector deadDetectThread = new DeadLockDetector();
      deadDetectThread.setDaemon(true);
      deadDetectThread.start();
    } else _log.info("Deadlock detector is disabled.");

    System.gc();

    _log.info(
        "Gameserver have started, used memory: "
            + SysUtil.getUsedMemory()
            + " / "
            + SysUtil.getMaxMemory()
            + " Mo.");
    _log.info("Maximum allowed players: " + Config.MAXIMUM_ONLINE_USERS);

    StringUtil.printSection("Login");
    LoginServerThread.getInstance().start();

    final SelectorConfig sc = new SelectorConfig();
    sc.MAX_READ_PER_PASS = Config.MMO_MAX_READ_PER_PASS;
    sc.MAX_SEND_PER_PASS = Config.MMO_MAX_SEND_PER_PASS;
    sc.SLEEP_TIME = Config.MMO_SELECTOR_SLEEP_TIME;
    sc.HELPER_BUFFER_COUNT = Config.MMO_HELPER_BUFFER_COUNT;

    final L2GamePacketHandler handler = new L2GamePacketHandler();
    _selectorThread = new SelectorThread<>(sc, handler, handler, handler, new IPv4Filter());

    InetAddress bindAddress = null;
    if (!Config.GAMESERVER_HOSTNAME.equals("*")) {
      try {
        bindAddress = InetAddress.getByName(Config.GAMESERVER_HOSTNAME);
      } catch (UnknownHostException e1) {
        _log.log(
            Level.SEVERE,
            "WARNING: The GameServer bind address is invalid, using all available IPs. Reason: "
                + e1.getMessage(),
            e1);
      }
    }

    try {
      _selectorThread.openServerSocket(bindAddress, Config.PORT_GAME);
    } catch (IOException e) {
      _log.log(Level.SEVERE, "FATAL: Failed to open server socket. Reason: " + e.getMessage(), e);
      System.exit(1);
    }
    _selectorThread.start();
  }

  public static void main(String[] args) throws Exception {
    _gameServer = new GameServer();
  }

  public static GameServer getInstance() {
    return _gameServer;
  }

  public SelectorThread<L2GameClient> getSelectorThread() {
    return _selectorThread;
  }
}
