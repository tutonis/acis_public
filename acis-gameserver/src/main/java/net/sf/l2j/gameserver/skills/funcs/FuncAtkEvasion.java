package net.sf.l2j.gameserver.skills.funcs;

import net.sf.l2j.gameserver.skills.Env;
import net.sf.l2j.gameserver.skills.Formulas;
import net.sf.l2j.gameserver.skills.Stats;
import net.sf.l2j.gameserver.skills.basefuncs.Func;

public class FuncAtkEvasion extends Func {
  static final FuncAtkEvasion _fae_instance = new FuncAtkEvasion();

  private FuncAtkEvasion() {
    super(Stats.EVASION_RATE, 0x10, null, null);
  }

  public static Func getInstance() {
    return _fae_instance;
  }

  @Override
  public void calc(Env env) {
    env.addValue(
        Formulas.BASE_EVASION_ACCURACY[env.getCharacter().getDEX()]
            + env.getCharacter().getLevel());
  }
}
